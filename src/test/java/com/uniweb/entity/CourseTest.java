package com.uniweb.entity;

import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

public class CourseTest {

    @Test
    public void courseEntityTest() {
        Course course = new Course(1, "New Course");

        Assert.assertNotNull(course);
        Assert.assertEquals(1, course.getId());

        course.setId(999);
        Assert.assertEquals(999, course.getId());

        course.setName("Course Updated");
        Assert.assertEquals("Course Updated", course.getName());

        Student student1 = new Student(1,123, "Test", "Student");
        Student student2 = new Student(2, 456, "John", "Wick");
        course.setStudents(Set.of(student1, student2));
        Assert.assertNotNull(course.getStudents());
    }
}
