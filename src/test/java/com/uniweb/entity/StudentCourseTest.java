package com.uniweb.entity;

import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

public class StudentCourseTest {

    @Test
    public void studentEntityTest() {
        StudentCourse studentCourse = new StudentCourse(1, 222, 333);

        Assert.assertNotNull(studentCourse);
        Assert.assertEquals(1, studentCourse.getId());
        Assert.assertEquals(222, studentCourse.getIdcourse());
        Assert.assertEquals(333, studentCourse.getIdstudent());
    }
}
