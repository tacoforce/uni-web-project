package com.uniweb.entity;

import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

public class StudentTest {

    @Test
    public void studentEntityTest() {
        Student student = new Student(1, 123, "Hulk", "Hogan");

        Assert.assertNotNull(student);
        Assert.assertEquals(1, student.getId());
        Assert.assertEquals(123, student.getEnrollment());
        Assert.assertEquals("Hulk", student.getName());
        Assert.assertEquals("Hogan", student.getLastname());

        Course course1 = new Course(1, "New Course");
        Course course2 = new Course(2, "Advance Gaming");
        student.setCourses(Set.of(course1, course2));

        Assert.assertNotNull(student.getCourses());
    }
}
