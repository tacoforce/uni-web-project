package com.uniweb.service;

import com.uniweb.entity.Course;
import com.uniweb.entity.Student;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RunWith(MockitoJUnitRunner.class)
public class CourseServiceTest {

    @Mock
    CourseService courseService;

    Course course;
    List<Course> courseList;
    Student student;

    @Before
    public void init() {
        course = new Course();
        course.setId(1);
        course.setName("Test Course");

        student = new Student(1, 123, "Student", "Test");
        course.setStudents(Set.of(student));

        courseList = new ArrayList<>();
        courseList.add(course);
    }

    @Test
    public void addCourseTest() {
        Mockito.when(courseService.addCourse(course)).thenReturn(course);
        courseService.addCourse(course);
        Mockito.verify(courseService, Mockito.times(1)).addCourse(course);
        Assert.assertNotNull(courseService.addCourse(course));
    }

    @Test
    public void deleteCourseTest() {
        courseService.deleteCourse(course.getId());
        Mockito.verify(courseService, Mockito.times(1)).deleteCourse(course.getId());
    }

    @Test
    public void getCoursesTest() {
        Mockito.when(courseService.getCourses()).thenReturn(courseList);
        Assert.assertNotNull(courseService.getCourses());
    }

    @Test
    public void getCourseTest() {
        Mockito.when(courseService.getCourse(1)).thenReturn(course);
        Assert.assertNotNull(courseService.getCourse(1));
        Assert.assertEquals(1, courseService.getCourse(1).getId());
    }

    @Test
    public void enrollStudentTest() {
        Mockito.when(courseService.enrollStudent(1, 1)).thenReturn(course);
        Assert.assertNotNull(courseService.enrollStudent(1, 1));
        Assert.assertEquals(true, courseService.enrollStudent(1, 1).getStudents().contains(student));
    }
}
