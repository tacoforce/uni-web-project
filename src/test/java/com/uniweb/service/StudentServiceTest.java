package com.uniweb.service;

import com.uniweb.entity.Course;
import com.uniweb.entity.Student;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RunWith(MockitoJUnitRunner.class)
public class StudentServiceTest {

    @Mock
    StudentService studentService;

    Course course;
    List<Student> studentList;
    Student student;

    @Before
    public void init() {
        course = new Course(1, "Test Course");
        student = new Student(1, 123, "Student", "Test");

        course.setStudents(Set.of(student));

        studentList = new ArrayList<>();
        studentList.add(student);
    }

    @Test
    public void addStudentTest() {
        Mockito.when(studentService.addStudent(student)).thenReturn(student);
        studentService.addStudent(student);
        Mockito.verify(studentService, Mockito.times(1)).addStudent(student);
        Assert.assertNotNull(studentService.addStudent(student));
    }

    @Test
    public void deleteStudentTest() {
        studentService.deleteStudent(student.getId());
        Mockito.verify(studentService, Mockito.times(1)).deleteStudent(student.getId());
    }

    @Test
    public void getStudentsTest() {
        Mockito.when(studentService.getStudents()).thenReturn(studentList);
        Assert.assertNotNull(studentService.getStudents());
    }

    @Test
    public void getStudentTest() {
        Mockito.when(studentService.getStudent(1)).thenReturn(student);
        Assert.assertNotNull(studentService.getStudent(1));
    }
}
