package com.uniweb.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uniweb.UniWebApplication;
import com.uniweb.entity.Student;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UniWebApplication.class)
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.DEFAULT)
public class StudentControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    private Student student = new Student(666, 123, "Student", "Test");

    @Test
    public void addStudentEndpointTest() throws Exception {
        mvc.perform(
            post("/students")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(student)))
            .andExpect(status().isOk());
    }

    @Test
    public void getStudentsEndpointTest() throws Exception {
        mvc.perform(get("/students")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteStudentEndpointTest() throws Exception {
        mvc.perform(delete("/students/666")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

    @Test
    public void getStudentEndpointTest() throws Exception {

        Student student = new Student(999, 123, "Harry", "Potter");

        mvc.perform(
            post("/students")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(student)))
            .andExpect(status().isOk());

        mvc.perform(get("/students/999")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mvc.perform(delete("/students/999")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

}
