package com.uniweb.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uniweb.UniWebApplication;
import com.uniweb.entity.Course;
import com.uniweb.entity.Student;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UniWebApplication.class)
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.DEFAULT)
public class CourseControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    private Course course = new Course(666, "Test Course");

    @Test
    public void addCourseEndpointTest() throws Exception {
        mvc.perform(
            post("/courses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(course)))
            .andExpect(status().isOk());
    }

    @Test
    public void getCoursesEndpointTest() throws Exception {
        mvc.perform(get("/courses")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

    @Test
    public void getCourseEndpointTest() throws Exception {
        mvc.perform(get("/courses/666")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

    @Test
    public void deleteCourseEndpointTest() throws Exception {
        mvc.perform(delete("/courses/666")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

}
