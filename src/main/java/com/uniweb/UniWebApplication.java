package com.uniweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UniWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(UniWebApplication.class, args);
    }
}
