package com.uniweb.util;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.uniweb.config.SerializerConfig;
import com.uniweb.entity.Course;

import java.util.List;

@JsonSerialize(using = SerializerConfig.class)
public class CustomResponse {

    public List<Course> courses;

    public CustomResponse(List<Course> courses) {
        this.courses = courses;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }
}
