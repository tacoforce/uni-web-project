package com.uniweb.service;

import com.uniweb.entity.Student;
import com.uniweb.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {

    @Autowired
    StudentRepository studentRepository;

    public Student addStudent(Student student) {
        return studentRepository.save(student);
    }

    public void deleteStudent(long idStudent) {
        studentRepository.delete(new Student(idStudent));
    }

    public List<Student> getStudents() {
        return (List<Student>) studentRepository.findAll();
    }

    public Student getStudent(long idStudent) {
        return studentRepository.findById(idStudent).get();
    }
}
