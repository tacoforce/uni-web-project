package com.uniweb.service;

import com.uniweb.entity.Course;
import com.uniweb.entity.StudentCourse;
import com.uniweb.repository.CourseRepository;
import com.uniweb.repository.StudentCourseRepository;
import com.uniweb.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseService {

    @Autowired
    CourseRepository courseRepository;

    @Autowired
    StudentCourseRepository studentCourseRepository;

    @Autowired
    StudentRepository studentRepository;

    public Course addCourse(Course course) {
        return courseRepository.save(course);
    }

    public void deleteCourse(long idCourse) {
        courseRepository.delete(new Course(idCourse));
    }

    public List<Course> getCourses() {
        return (List<Course>) courseRepository.findAll();
    }

    public Course getCourse(long idCourse) {
        return courseRepository.findById(idCourse).get();
    }

    public Course enrollStudent(long idCourse, long idStudent) {

        if (courseRepository.findById(idCourse).get() != null) {
            if (studentRepository.findById(idStudent).get() != null) {
                studentCourseRepository.save(new StudentCourse(idCourse, idStudent));
            }
        }
        return courseRepository.findById(idCourse).get();
    }
}
