package com.uniweb.repository;

import com.uniweb.entity.StudentCourse;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface StudentCourseRepository extends CrudRepository<StudentCourse, Long> {
}
