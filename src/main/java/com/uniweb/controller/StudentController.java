package com.uniweb.controller;

import com.uniweb.entity.Student;
import com.uniweb.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class StudentController {

    @Autowired
    StudentService studentService;

    @GetMapping("/students")
    public List<Student> getStudents() {
        return studentService.getStudents();
    }

    @GetMapping("/students/{idStudent}")
    public Student getStudent(
        @PathVariable long idStudent
    ) {
        return studentService.getStudent(idStudent);
    }

    @PostMapping("/students")
    public Student addStudent(
        @RequestBody Student student
    ) {
        return studentService.addStudent(student);
    }

    @DeleteMapping("/students/{idStudent}")
    public void deleteStudent(
        @PathVariable long idStudent
    ) {
        studentService.deleteStudent(idStudent);
    }
}
