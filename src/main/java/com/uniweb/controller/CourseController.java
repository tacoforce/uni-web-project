package com.uniweb.controller;

import com.uniweb.entity.Course;
import com.uniweb.service.CourseService;
import com.uniweb.util.CustomResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CourseController {

    @Autowired
    CourseService courseService;

    @GetMapping("/courses")
    public ResponseEntity<CustomResponse> getCourses() {
        return new ResponseEntity(new CustomResponse(courseService.getCourses()), null,  HttpStatus.OK);
    }

    @GetMapping("/courses/{idCourse}")
    public Course getCourse(
        @PathVariable long idCourse
    ) {
        return courseService.getCourse(idCourse);
    }

    @PostMapping("/courses")
    public Course addCourse(
        @RequestBody Course course
    ) {
        return courseService.addCourse(course);
    }

    @DeleteMapping("/courses/{idCourse}")
    public void deleteCourse(
        @PathVariable long idCourse
    ) {
        courseService.deleteCourse(idCourse);
    }

    @PutMapping("/courses/{idCourse}/student/{idStudent}")
    public Course enrollStudent(
        @PathVariable long idCourse,
        @PathVariable long idStudent
    ) {
        return courseService.enrollStudent(idCourse, idStudent);
    }
}
