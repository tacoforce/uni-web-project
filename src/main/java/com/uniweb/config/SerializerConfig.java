package com.uniweb.config;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.uniweb.entity.Course;
import com.uniweb.util.CustomResponse;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class SerializerConfig extends JsonSerializer<CustomResponse> {

    @Override
    public void serialize(CustomResponse response, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartArray();
        for (Course course : response.getCourses()) {
            gen.writeObject(course);
        }
        gen.writeEndArray();
    }

}
