package com.uniweb.entity;

import lombok.Data;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "student_course", schema = "university")
public class StudentCourse {
    @Basic
    @Column(name = "idcourse")
    private long idcourse;
    @Id
    @Column(name = "id")
    private long id;
    @Basic
    @Column(name = "idstudent")
    private long idstudent;

    public StudentCourse() {}

    public StudentCourse(long id, long idCourse, long idStudent) {
        this.id = id;
        this.idcourse = idCourse;
        this.idstudent = idStudent;
    }

    public StudentCourse(long idCourse, long idStudent) {
        this.idcourse = idCourse;
        this.idstudent = idStudent;
    }
}
