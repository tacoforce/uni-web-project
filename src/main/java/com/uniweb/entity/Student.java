package com.uniweb.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@Table(name = "student", schema = "university")
public class Student {
    @Id
    @Column(name = "id")
    private long id;
    @Basic
    @Column(name = "enrollment")
    private long enrollment;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "lastname")
    private String lastname;
    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "student_course",
            joinColumns = { @JoinColumn(name = "idstudent", referencedColumnName = "id") },
            inverseJoinColumns = { @JoinColumn(name = "idcourse", referencedColumnName = "id") })
    Set<Course> courses = new HashSet<>();

    public Student() {
    }

    public Student(long id) {
        this.id = id;
    }

    public Student(long id, long enrollment, String name, String lastname) {
        this.id = id;
        this.enrollment = enrollment;
        this.name = name;
        this.lastname = lastname;
    }
}
